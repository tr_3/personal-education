# personal-education
The goal of this project is to keep myself learning as much as possible, in a variety of different areas.  This repo is tracked via Beeminder (see [https://www.beeminder.com/thils2/personal-education](https://www.beeminder.com/thils2/personal-education))

**What I'm working on now:**

- [FreeCodeCamp](https://www.freecodecamp.org/cleverdream)
- Learning [Isadora](https://troikatronix.com/)


See separate pages in GitHub for status on each project.